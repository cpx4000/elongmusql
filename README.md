# eLongMuSQL .NET Library

High level SQL operation lightweight .NET Library. 
Ultra fast implementations using object oriented data manipulation (no queries). 
Tables are declared as classes.

## FUNCTIONS


```
	InsertEntry<T>(T entry)
	UpdateEntry<T>(T entry)
	DeleteEntry<T>(T entry)
	GetTable<T>()
	GetTableWithCondition<T>(string condition)
	.
	.
	.
	and many more...
```


## Getting started


```
       public event PropertyChangedEventHandler PropertyChanged;
       private eLongMuSQL.MuSQL _muSQLclient { get; set; }
       private IConfigurations _config { get; set; }

       public MainWindow()
        {
            DataContext = this;
            // prepare the configuration objects
            // you can do your own as long it implements the IConfigurations interface
            _config = new Configurations()
            {
                EnableDebugInfo = false,
                SQLDataBase = "database",
                SQLAddress = "localhost",
                SQLUser = "sa",
                SQLPass = "qwertypass123"
            };

            // init the muSQLclient with the above created configuration object
            _muSQLclient = new MuSQL(_config);

            // get a list of objects (query a table)
            var entireTable = _muSQLclient.GetTable<TB__STATO_STAZIONE>();
            var filteredTable = _muSQLclient.GetTableWithCondition<TB__STATO_STAZIONE>(" where FkStation='10011'");

            var newEntry1 = new TB__STATO_STAZIONE()
            {
                DateStart = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"),
                FkStation = 10012,
                FkStatoStazione = -10,
                Secondi = -1
            };
            var newEntry2 = new TB__STATO_STAZIONE()
            {
                DateStart = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"),
                FkStation = 10012,
                FkStatoStazione = -11,
                Secondi = -11
            };

            // insert new entry in a table 
            _muSQLclient.InsertEntry(newEntry1);

            // creating a list of entries to be inserted
            var multipleEntries = new List<TB__STATO_STAZIONE>()
            {
                new TB__STATO_STAZIONE(){ DateStart = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), FkStation = 10013, FkStatoStazione =-13, Secondi = -13 },
                new TB__STATO_STAZIONE(){ DateStart = DateTime.Now.AddMilliseconds(1).ToString("yyyy-MM-dd HH:mm:ss.fff"), FkStation = 10014, FkStatoStazione =-14, Secondi = -14 },
            };

            // insert multiple new entries
            _muSQLclient.InsertEntries(multipleEntries);

            newEntry1.FkStation = 10101;

            // update existing entry
            _muSQLclient.UpdateEntry(newEntry1);

            // delete existing entry
            _muSQLclient.DeleteEntry(newEntry1);

            // create new table 
            _muSQLclient.CreateTable<DemoTable>();
            // insert new entry in table
            var demoTable = new DemoTable() { Id = 1, Name = "default name" };
            _muSQLclient.InsertEntry(demoTable);

            // testing again update of an existing entry
            demoTable.Name = "newName";
            _muSQLclient.UpdateEntry(demoTable);

            _muSQLclient.DeleteEntry(demoTable);

            // binded collection
            MuSQLDataGrid = new ObservableCollection<TB__STATO_STAZIONE>();

            InitializeComponent();
        }

```

## DECLARING TABLES EXAMPLE

```
    [SQLTable("DemoTable")]
    class DemoTable
    {
        [SQLColumn(SQLColumnType.BigInt,"Id",true,true)]
        public int Id { get; set; }

        [SQLColumn(SQLColumnType.Varchar,"Name",false,false)]
        public string Name { get; set; }
    }
```

## DECLARING LINKED TABLES EXAMPLE

```
[SQLTable(nameof(FinishedGoods))]
    class FinishedGoods
    {
        [SQLColumn(SQLColumnType.BigInt, nameof(Id), true, true)]
        public Int64 Id { get; set; }


        [SQLColumn(SQLColumnType.Varchar, nameof(Name), false, false)]
        public string Name { get; set; }


        [SQLTableLink(typeof(FinishedGoodsBOM), nameof(Id), nameof(FinishedGoodsBOM.IdFinishedGood))]
        public List<FinishedGoodsBOM> _FinishedGoodsBOM { get; set; }

        public FinishedGoods()
        { }
    }

    [SQLTable(nameof(AssyComponent))]
    class AssyComponent
    {
        [SQLColumn(SQLColumnType.BigInt, nameof(Id), true, true)]
        public Int64 Id { get; set; }


        [SQLColumn(SQLColumnType.Varchar, nameof(Name), false, false)]
        public string Name { get; set; }

        public AssyComponent()
        { }
    }

    [SQLTable(nameof(FinishedGoodsBOM))]
    class FinishedGoodsBOM
    {

        [SQLColumn(SQLColumnType.BigInt, nameof(Id), true, true)]
        public Int64 Id { get; set; }


        [SQLColumn(SQLColumnType.BigInt, nameof(IdFinishedGood), true, false)]
        public Int64 IdFinishedGood { get; set; }


        [SQLColumn(SQLColumnType.BigInt, nameof(IdComponent), true, false)]
        public Int64 IdComponent { get; set; }


        [SQLTableLink(typeof(AssyComponent), nameof(IdComponent), nameof(AssyComponent.Id))]
        public List<AssyComponent> _AssyComponents { get; set; }

        public FinishedGoodsBOM()
        { }
    }
```