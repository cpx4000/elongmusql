﻿using CustomSQLInterface.tables;
using eLongMuSQL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CustomSQLInterface
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region -- PROPERTIES --
        public event PropertyChangedEventHandler PropertyChanged;
        private static eLongMuSQL.MuSQL _muSQLclient { get; set; }
        private IConfigurations _config { get; set; }

        /// <summary>
        /// Collection binded to datagrid
        /// check TB__STATO_STAZIONE class to see how a sql table is defined in MuSQL
        /// </summary>
        public ObservableCollection<TB__STATO_STAZIONE> MuSQLDataGrid { get; set; }

        #endregion

        #region -- CONSTRUCTOR --
        public MainWindow()
        {
            DataContext = this;
            // prepare the configuration objects
            // you can do your own as long it implements the IConfigurations interface
            _config = new Configurations()
            {
                EnableDebugInfo = false,
                SQLDataBase = "database",
                SQLAddress = "localhost",
                SQLUser = "sa",
                SQLPass = "qwertypass123"
            };

            // init the muSQLclient with the above created configuration object
            _muSQLclient = new MuSQL(_config);

            // get a list of objects (query a table)
            var entireTable = _muSQLclient.GetTable<TB__STATO_STAZIONE>();
            var filteredTable = _muSQLclient.GetTableWithCondition<TB__STATO_STAZIONE>(" where FkStation='10011'");

            var newEntry1 = new TB__STATO_STAZIONE()
            {
                DateStart = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"),
                FkStation = 10012,
                FkStatoStazione = -10,
                Secondi = -1
            };
            var newEntry2 = new TB__STATO_STAZIONE()
            {
                DateStart = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"),
                FkStation = 10012,
                FkStatoStazione = -11,
                Secondi = -11
            };

            // insert new entry in a table 
            _muSQLclient.InsertEntry(newEntry1);

            // creating a list of entries to be inserted
            var multipleEntries = new List<TB__STATO_STAZIONE>()
            {
                new TB__STATO_STAZIONE(){ DateStart = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), FkStation = 10013, FkStatoStazione =-13, Secondi = -13 },
                new TB__STATO_STAZIONE(){ DateStart = DateTime.Now.AddMilliseconds(1).ToString("yyyy-MM-dd HH:mm:ss.fff"), FkStation = 10014, FkStatoStazione =-14, Secondi = -14 },
            };

            // insert multiple new entries
            _muSQLclient.InsertEntries(multipleEntries);

            newEntry1.FkStation = 10101;

            // update existing entry
            _muSQLclient.UpdateEntry(newEntry1);

            // delete existing entry
            _muSQLclient.DeleteEntry(newEntry1);

            // create new table 
            _muSQLclient.CreateTable<DemoTable>();
            // insert new entry in table
            var demoTable = new DemoTable() { Id = 1, Name = "default name" };
            _muSQLclient.InsertEntry(demoTable);

            // testing again update of an existing entry
            demoTable.Name = "newName";
            _muSQLclient.UpdateEntry(demoTable);

            _muSQLclient.DeleteEntry(demoTable);

            // binded collection
            MuSQLDataGrid = new ObservableCollection<TB__STATO_STAZIONE>();


            // example of tables linked together
            CreateDemoData();
            GetAndPrintDemoData();


            InitializeComponent();
        }

        private void GetAndPrintDemoData()
        {
            var finishedGoods = _muSQLclient.GetTable<FinishedGoods>();

            foreach (var finishedGood in finishedGoods)
            {
                Console.WriteLine($"finished good : {finishedGood.Name}");
                Console.WriteLine("     has components:");
                
                foreach (var BomInfo in finishedGood._FinishedGoodsBOM)
                    Console.WriteLine($"component:{BomInfo._AssyComponents.First().Name}");

                Console.WriteLine();
            }
        }

        private static void CreateDemoData()
        {
            var finishedGood = new FinishedGoods()
            {
                Id = 1,
                Name = "FG1",
            };

            var component1 = new AssyComponent()
            {
                Id = 1,
                Name = "Component1"
            };

            var component2 = new AssyComponent()
            {
                Id = 2,
                Name = "Component2"
            };

            var connection1 = new FinishedGoodsBOM()
            {
                Id=1,
                IdFinishedGood = 1,
                IdComponent =1
            };

            var connection2 = new FinishedGoodsBOM()
            {
                Id = 2,
                IdFinishedGood = 1,
                IdComponent = 2
            };

            CreateTableIfNotExist<AssyComponent>();
            CreateTableIfNotExist<FinishedGoods>();
            CreateTableIfNotExist<FinishedGoodsBOM>();

            _muSQLclient.InsertEntry(finishedGood);
            _muSQLclient.InsertEntries(new List<AssyComponent>() { component1, component2 });
            _muSQLclient.InsertEntries(new List<FinishedGoodsBOM>() { connection1, connection1 });
        }

        private static void CreateTableIfNotExist<T>() where T: new()
        {
            if (!_muSQLclient.TableExists<T>())
                _muSQLclient.CreateTable<T>();
        }

        #endregion

    }


    [SQLTable(nameof(DemoTable))]
    class DemoTable
    {
        [SQLColumn(SQLColumnType.BigInt, nameof(Id), true, true)]
        public Int64 Id { get; set; }


        [SQLColumn(SQLColumnType.Varchar, nameof(Name), false, false)]
        public string Name { get; set; }

        public DemoTable()
        { }
    }

    [SQLTable(nameof(FinishedGoods))]
    class FinishedGoods
    {
        [SQLColumn(SQLColumnType.BigInt, nameof(Id), true, true)]
        public Int64 Id { get; set; }


        [SQLColumn(SQLColumnType.Varchar, nameof(Name), false, false)]
        public string Name { get; set; }


        [SQLTableLink(typeof(FinishedGoodsBOM), nameof(Id), nameof(FinishedGoodsBOM.IdFinishedGood))]
        public List<FinishedGoodsBOM> _FinishedGoodsBOM { get; set; }

        public FinishedGoods()
        { }
    }

    [SQLTable(nameof(AssyComponent))]
    class AssyComponent
    {
        [SQLColumn(SQLColumnType.BigInt, nameof(Id), true, true)]
        public Int64 Id { get; set; }


        [SQLColumn(SQLColumnType.Varchar, nameof(Name), false, false)]
        public string Name { get; set; }

        public AssyComponent()
        { }
    }

    [SQLTable(nameof(FinishedGoodsBOM))]
    class FinishedGoodsBOM
    {

        [SQLColumn(SQLColumnType.BigInt, nameof(Id), true, true)]
        public Int64 Id { get; set; }


        [SQLColumn(SQLColumnType.BigInt, nameof(IdFinishedGood), true, false)]
        public Int64 IdFinishedGood { get; set; }


        [SQLColumn(SQLColumnType.BigInt, nameof(IdComponent), true, false)]
        public Int64 IdComponent { get; set; }


        [SQLTableLink(typeof(AssyComponent), nameof(IdComponent), nameof(AssyComponent.Id))]
        public List<AssyComponent> _AssyComponents { get; set; }

        public FinishedGoodsBOM()
        { }
    }
}
