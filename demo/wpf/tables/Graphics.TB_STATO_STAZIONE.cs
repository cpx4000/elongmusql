﻿using eLongMuSQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomSQLInterface.tables
{

    [SQLTable("Graphics.TB__STATO_STAZIONE")]
    public class TB__STATO_STAZIONE
    {
        [SQLColumn(SQLColumnType.Varchar, "DateStart", true, true)]
        public string DateStart { get; set; }

        
        [SQLColumn(SQLColumnType.BigInt, "FkStation", true, false)]
        public Int64 FkStation { get; set; }


        [SQLColumn(SQLColumnType.BigInt, "FkStatoStazione", true, false)]
        public Int64 FkStatoStazione { get; set; }


        [SQLColumn(SQLColumnType.Int, "Secondi", true, false)]
        public Int64 Secondi { get; set; }
    }
}
