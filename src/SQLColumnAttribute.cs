﻿using System;

namespace eLongMuSQL
{
    public class SQLUsedOnlyInCreateTableAttribute : Attribute
    {
    }
    public class SQLColumnAttribute : Attribute
    {
        public SQLColumnType ColumnType { get; set; }
        public bool isPrimaryKey { get; set; } = false;
        public bool NotNULL { get; set; } = false;
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ColumnType">SQL DataType of the column in the sql table</param>
        /// <param name="Name">Name of the column in the sql table</param>
        /// <param name="NotNULL">Should force NotNULL values only</param>
        /// <param name="isPrimaryKey">Is Primary Key</param>
        public SQLColumnAttribute(SQLColumnType ColumnType, string Name, bool NotNULL, bool isPrimaryKey)
        {
            this.ColumnType = ColumnType;
            this.NotNULL = NotNULL;
            this.Name = Name;
            this.isPrimaryKey = isPrimaryKey;
        }
    }


    public class SQLTableLinkAttribute : Attribute
    {
        public Type _TableType { get; set; }
        public string id1;
        public string id2; 

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_tableType">Type of the table we're going to look for connected elements</param>
        /// <param name="id1">Id of the Owner/Parent table</param>
        /// <param name="id2">Id of the other table</param>
        public SQLTableLinkAttribute(Type _tableType,string id1, string id2)
        {
            this.id1 = id1;
            this.id2 = id2;
            _TableType = _tableType;
        }
    }

}
